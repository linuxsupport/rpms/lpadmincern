#!/bin/bash

for P in $(lpstat -p | awk '{print $2}'); do
 lpoptions -p $P -l | grep -i $1
done
