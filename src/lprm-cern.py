#!/usr/bin/python3 -W ignore::DeprecationWarning

#
# lprm-xprint: integration of CUPS and CERN xprint
#
# lprm [ -E ] [ - ] [ -P destination ] [ job ID(s) ]

import sys
import os
import getopt
import time
import pwd
import subprocess
# Just so we do not confuse module name with function
from socket import *

testserverfile = "/usr/share/printconf/cern/PrintTests"

lpqcups = "/usr/bin/lprm.cups"
lpstatcups = "/usr/bin/lpstat.cups"
lpqargs = [" "]
lpqargsstr = " "
lpdomain = ".cern.ch"
lpserver = ".print"+lpdomain

for a in sys.argv[1:]:
  lpqargs.append(a)
  lpqargsstr += a
  lpqargsstr += " "


# default printer finding: $LPDEST -> $PRINTER -> lpstat -d
def get_default_printer(lpstat):
  if "LPDEST" in os.environ:
    return os.environ["LPDEST"]
  else:
    if "PRINTER" in os.environ:
      return os.environ["PRINTER"]
    else:
      try:
        output = os.popen("LC_ALL=C " + lpstat + " -d").readlines ()[0]
        return output.split(": ")[1].strip ()
      except Exception as e:
        print(e)
        pass
  return ""

# check remote lpr server for result
# NEED more error handling
def query_lpr(printserver, msg):

  ## This is to check that given printer is defined in DNS
  ## which is then assumed to be managed by central service
  ## otherwise we service local printer

  try:
    gethostbyname(printserver)
  except Exception as e:
    print(e)
    pass
    return

  try:
    s = socket(AF_INET, SOCK_STREAM)
    s.settimeout(5)
    s.connect((printserver, 515))
    s.send(msg.encode())
    out = s.recv(8192)
# this is a hack ....
#    i = 1
#    while i < 5:
#      try:
#       out+= s.recv(8192)
#       i+=1
#      except:
#       break
    sys.stdout.write(out.decode())
  except KeyboardInterrupt:
    sys.exit(0)
    pass
  except Exception as e:
    print(e)
    sys.stderr.write("lp: error - connecting to: " + printserver + "\n")
    sys.exit(1)
    pass

    s.close()
    return

# check local CUPS server
# NEED more error handling
def query_cups(lpqcomm, lpqargs):
  try:
    p = subprocess.Popen([lpqcomm, lpqargs], stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE, close_fds=True, universal_newlines=True)
    sout, serr = p.communicate()
    print(sout)
  except KeyboardInterrupt:
    sys.exit(0)

  if serr.startswith("lprm: Unknown destination"):
    sys.stderr.write(serr)
    sys.exit(1)


# Construct lpq message to pass to lpr server,
# according to RFC 1179
#\005 | Queue | SP | Agent | SP | List | LF
#
def make_lpr_msg(printer, user,job,new):
  if new:
    msg = "\005" + printer + " " + user + " "
  else:
    msg = "\005" + printer + " " + user + "\n"

  if job != '':
    msg += job + "\n"
  else:
    msg += "\n"

  return msg

def checkiftest(printer, server):
  try:
    os.stat(testserverfile)
    fH = os.popen("/usr/bin/lpstat -v " + printer)
    for l in fH.readlines():
      srv = l.split('//')[1]
      srv = srv.split('/')[0]
      break
    gethostbyname(srv)
    if srv == None or srv == "":
      return (False, printer + server)
  except Exception as e:
    return (True, printer + server)

  print("Info: Using TEST print server: " + srv)
  return (True, srv)

# If we are not at CERN pass all handling to CUPS asap
if not getfqdn().endswith(lpdomain):
   os.execv(lpqcups, lpqargs)  

try:
  opts, args = getopt.getopt(sys.argv[1:], "EP:", [])
except getopt.GetoptError:
  print("Usage: lprm [ -E ] [ -P destination ] [ - ] [ job ID(s) ]")
  sys.exit(1)

long = 0
interval = 0
user = pwd.getpwuid(os.getuid())[0]
printer = ''

for option, argument in opts:
  if option == "-P":
     printer = str(argument)

#print "leftovers:" + str(args)
try:
  job = str(args[0])
except:
  job = ''
  pass

if printer == '':
  printer = get_default_printer(lpstatcups)
  if printer == '':
    print("lprm: Unable to cancel job(s)!")
    sys.exit(1)

(new, server) = checkiftest(printer, lpserver)

if interval:
  while 1:
    try:
      query_cups(lpqcups, lpqargsstr)
      query_lpr(server, make_lpr_msg(printer,user,job,new)) 
      time.sleep(interval)
    except KeyboardInterrupt:
      sys.exit(0)
      pass
else:
  query_cups(lpqcups, lpqargsstr)
  query_lpr(server, make_lpr_msg(printer,user,job,new)) 

sys.exit(0)
