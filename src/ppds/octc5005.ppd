*PPD-Adobe: "4.3"
*%==========================================================================================
*%
*%          PostScript(R) Printer Description file for the Oc� TCS500
*%
*%          Copyright 2001-2010 Oc�.
*%
*%==========================================================================================
*%
*%  English with roman fonts
*%
*%==========================================================================================
*%Platform: Windows
*FileVersion: "1.16"
*FormatVersion: "4.3"
*LanguageEncoding: ISOLatin1
*LanguageVersion: English
*Manufacturer: "Oce"
*ModelName: "Oce TCS500 PS"
*PCFileName: "octc5005.PPD"
*Product: "(TCS500)"
*PSVersion: "(3011.106) 2071"
*ShortNickName: "Oce TCS500 PS"
*NickName: "Oce TCS500 PS"

*% ========== 5.4 Installable Options, and Constraints ======================================

*OpenGroup: InstallableOptions

*OpenUI *OCOption1/Roll 2: Boolean
*DefaultOCOption1: True
*OCOption1 True/Installed: ""
*OCOption1 False/Not Installed: ""
*CloseUI: *OCOption1

*OpenUI *OCOption2/Roll 3: Boolean
*DefaultOCOption2: False
*OCOption2 True/Installed: ""
*OCOption2 False/Not Installed: ""
*CloseUI: *OCOption2

*OpenUI *OCOptionEsteFolder/Es-Te Folder: Boolean
*DefaultOCOptionEsteFolder: False
*OCOptionEsteFolder True/Installed: ""
*OCOptionEsteFolder False/Not Installed: ""
*CloseUI: *OCOptionEsteFolder

*CloseGroup: InstallableOptions

*%==========Constraints================================================================

*UIConstraints: *OCOption1 False *InputSlot Roll2
*UIConstraints: *InputSlot Roll2 *OCOption1 False 

*UIConstraints: *OCOption2 False *InputSlot Roll3
*UIConstraints: *InputSlot Roll3 *OCOption2 False

*%===========Es-Te Folder==============================
*UIConstraints: *OCOptionEsteFolder False *FoldWhen EndOfPage
*UIConstraints: *FoldWhen EndOfPage *OCOptionEsteFolder False

*%========Constraints media types >110gm2==============
*UIConstraints: *FoldWhen EndOfPage *MediaType StabilizedPaper_90-110gr
*UIConstraints: *MediaType StabilizedPaper_90-110gr *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType OcePhotogloss_175gr
*UIConstraints: *MediaType OcePhotogloss_175gr *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType TranslucentBond_18lb
*UIConstraints: *MediaType TranslucentBond_18lb *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType DoubleMattFilm_95mu
*UIConstraints: *MediaType DoubleMattFilm_95mu *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType TracingPaper_90gr
*UIConstraints: *MediaType TracingPaper_90gr *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType ColorVellum_20lb
*UIConstraints: *MediaType ColorVellum_20lb *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType MonochromeVellum_20lb
*UIConstraints: *MediaType MonochromeVellum_20lb *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType TracingPaper_75gm2
*UIConstraints: *MediaType TracingPaper_75gm2 *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType ThinMattFilm_50mu
*UIConstraints: *MediaType ThinMattFilm_50mu *FoldWhen EndOfPage

*UIConstraints: *FoldWhen EndOfPage *MediaType Custom3
*UIConstraints: *MediaType Custom3 *FoldWhen EndOfPage

*%UIConstraints: *InputSlot Roll1 *MediaType
*%UIConstraints: *MediaType *InputSlot Roll1
*%UIConstraints: *InputSlot Roll2 *MediaType
*%UIConstraints: *MediaType *InputSlot Roll2

*%if OCColorMode Gray, no color feel at all (except none automatically).

*UIConstraints: *OCColorMode Gray *OCColorFeel

*%if OCColorFeel is NOT OCEnhancedColors, then no RGBInput data, no CMYKInputData, no RenderingIntent at all

*UIConstraints: *OCColorFeel OceCAD *OCRGBInputData
*UIConstraints: *OCColorFeel OceCAD *OCCMYKInputData
*UIConstraints: *OCColorFeel OceCAD *OCRenderingIntent

*UIConstraints: *OCColorFeel None *OCRGBInputData
*UIConstraints: *OCColorFeel None *OCCMYKInputData
*UIConstraints: *OCColorFeel None *OCRenderingIntent

*UIConstraints: *OCColorFeel PrinterDefault *OCRGBInputData
*UIConstraints: *OCColorFeel PrinterDefault *OCCMYKInputData
*UIConstraints: *OCColorFeel PrinterDefault *OCRenderingIntent

*UIConstraints: *OCColorFeel InApplication *OCRGBInputData
*UIConstraints: *OCColorFeel InApplication *OCCMYKInputData
*UIConstraints: *OCColorFeel InApplication *OCRenderingIntent

*UIConstraints: *OCColorFeel SimulatedOceTCS400 *OCRGBInputData
*UIConstraints: *OCColorFeel SimulatedOceTCS400 *OCCMYKInputData
*UIConstraints: *OCColorFeel SimulatedOceTCS400 *OCRenderingIntent

*UIConstraints: *OCColorFeel SimulatedVivid *OCRGBInputData
*UIConstraints: *OCColorFeel SimulatedVivid *OCCMYKInputData
*UIConstraints: *OCColorFeel SimulatedVivid *OCRenderingIntent

*UIConstraints: *OCColorFeel SimulatedMatchScreenColors *OCRGBInputData
*UIConstraints: *OCColorFeel SimulatedMatchScreenColors *OCCMYKInputData
*UIConstraints: *OCColorFeel SimulatedMatchScreenColors *OCRenderingIntent

*UIConstraints: *OCColorFeel SimulatedNoCorrection *OCRGBInputData
*UIConstraints: *OCColorFeel SimulatedNoCorrection *OCCMYKInputData
*UIConstraints: *OCColorFeel SimulatedNoCorrection *OCRenderingIntent

*%if OCColorFeel prompts for an Oce enhanced color choice, do not leave the choices to Not Applicable

*%UIConstraints: *OCColorFeel OCEnhancedColors *OCRGBInputData None
*%UIConstraints: *OCColorFeel OCEnhancedColors *OCCMYKInputData None
*%UIConstraints: *OCColorFeel OCEnhancedColors *OCRenderingIntent None

*% ========== 5.5 Basic Device Capabilities =================================================

*ColorDevice: True
*DefaultColorSpace: RGB
*FileSystem: True
*?FileSystem: "
false
(*) {
   /DevDict exch currentdevparams def
   DevDict /Writeable known {DevDict /Writeable get} {false} ifelse
   DevDict /Mounted   known {DevDict /Mounted   get} {false} ifelse
   DevDict /HasNames  known {DevDict /HasNames  get} {false} ifelse
   and and {pop true} if
} 128 string /IODevice resourceforall
{(True)} {(False)} ifelse ="
*End
*LanguageLevel: "3"
*Throughput: "22"
*TTRasterizer: Type42
*?TTRasterizer: "(Type42) ="

*% ========== 5.6 System Management =========================================================

*FreeVM: "44709876"
*VMOption 50meg: "44709876"
*Password: "()" 

*ExitServer: "
count 1 ge {true exc startjob} {false} ifelse
not {
   (ERROR: *ExitServer cannot start unencapsulated job.) =
   (       Password is probably invalid.) =
} if"
*End

*SuggestedJobTimeout: "0"
*SuggestedManualFeedTimeout: "60"
*SuggestedWaitTimeout: "0"
*PrintPSErrors: False

*% ========== 5.7 Emulations and Protocols ==================================================

*% ========== 5.8 Features Accessible Only Through Job Control Language =====================

*% ========== 5.9 Resolution and Appearance Control =========================================

*DefaultResolution: 600dpi
*Resolution 600dpi: ""

*% ========== 5.10 Gray Levels and Halftoning ===============================================

*ContoneOnly: False
*DefaultHalftoneType: 1
*ScreenAngle: "45"
*ScreenFreq: "90"
*DefaultScreenProc: Dot
*ScreenProc Dot: "{180 --mul-- --cos-- --exch-- 180 --mul-- --cos-- --add-- 2 --div--}
bind"
*End

*DefaultTransfer: Null

*% ========== 5.11 Color Adjustment =========================================================

*ColorRenderDict DefaultColorRendering: "/DefaultColorRendering /ColorRendering findresource setcolorrendering"
*%ColorRenderDict RelativeColorimetric.none.DefaultHalftone: "/RelativeColorimetric.none.DefaultHalftone /ColorRendering findresource setcolorrendering"
*ColorRenderDict Monochrome.none.DefaultHalftone: "/Monochrome.none.DefaultHalftone /ColorRendering findresource setcolorrendering"
*ColorRenderDict US_Web_Coated_SWOP-Per: "/US_Web_Coated_SWOP-Per /ColorRendering findresource setcolorrendering"

*RenderingIntent: Monochrome
*RenderingIntent: RelativeColorimetric

*PageDeviceName: none

*HalftoneName: true

*% ========== 5.14 Media Selection ==========================================================

*OpenUI *InputSlot: PickOne
*OrderDependency: 50.1 AnySetup *InputSlot
*DefaultInputSlot: Unknown
*InputSlot Roll1/Roll 1: "<< /MediaPosition 1 >> setpagedevice"
*InputSlot Roll2/Roll 2: "<< /MediaPosition 2 >> setpagedevice"
*InputSlot Roll3/Roll 3: "<< /MediaPosition 3 >> setpagedevice"
*CloseUI: *InputSlot

*OpenUI *PageSize: PickOne
*OrderDependency: 50.2 AnySetup *PageSize
*DefaultPageSize: A0
*PageSize A0/A0: 				"<< /PageSize [2384 3370] >> setpagedevice"
*PageSize AnsiA/A 8.5"x11": 			"<< /PageSize [612 792] >> setpagedevice"
*PageSize AnsiB/B 11"x17": 			"<< /PageSize [792 1224] >> setpagedevice"
*PageSize AnsiC/C 17"x22": 			"<< /PageSize [1224 1584] >> setpagedevice"
*PageSize AnsiD/D 22"x34": 			"<< /PageSize [1584 2448] >> setpagedevice"
*PageSize AnsiE/E 34"x44": 			"<< /PageSize [2448 3168] >> setpagedevice"
*PageSize ARCHA/A+ 9"x12": 			"<< /PageSize [648 864] >> setpagedevice"
*PageSize ARCHB/B+ 12"x18": 			"<< /PageSize [864 1296] >> setpagedevice"
*PageSize ARCHC/C+ 18"x24": 			"<< /PageSize [1296 1728] >> setpagedevice"
*PageSize ARCHD/D+ 24"x36": 			"<< /PageSize [1728 2592] >> setpagedevice"
*PageSize ARCHE/E+ 36"x48": 			"<< /PageSize [2592 3456] >> setpagedevice"
*PageSize A4/A4: 				"<< /PageSize [595 842]>> setpagedevice"
*PageSize A3/A3: 				"<< /PageSize [842 1191] >> setpagedevice"
*PageSize A2/A2: 				"<< /PageSize [1191 1684] >> setpagedevice"
*PageSize A1/A1: 				"<< /PageSize [1684 2384] >> setpagedevice"
*PageSize ISOB2/B2 500x707mm: 			"<< /PageSize [1417 2004] >> setpagedevice"
*PageSize ISOB1/B1 707x1000mm: 			"<< /PageSize [2004 2835] >> setpagedevice"
*PageSize inch30/30"x42":   			"<< /PageSize [2160 3024] >> setpagedevice"
*PageSize mm700/700x1000mm: 			"<< /PageSize [1984 2835] >> setpagedevice"
*PageSize mm500/500x700mm: 			"<< /PageSize [1417 1984] >> setpagedevice"
*PageSize A0EXT/A0 Oversize: 			"<< /PageSize [2384 3370] >> setpagedevice"
*PageSize AnsiAEXT/A 8.5"x11" Oversize: 	"<< /PageSize [612 792] >> setpagedevice"
*PageSize AnsiBEXT/B 11"x17" Oversize: 		"<< /PageSize [792 1224] >> setpagedevice"
*PageSize AnsiCEXT/C 17"x22" Oversize: 		"<< /PageSize [1224 1584] >> setpagedevice"
*PageSize AnsiDEXT/D 22"x34" Oversize: 		"<< /PageSize [1584 2448] >> setpagedevice"
*PageSize AnsiEEXT/E 34"x44" Oversize: 		"<< /PageSize [2448 3168] >> setpagedevice"
*PageSize ARCHAEXT/A+ 9"x12" Oversize: 		"<< /PageSize [648 864] >> setpagedevice"
*PageSize ARCHBEXT/B+ 12"x18" Oversize: 	"<< /PageSize [864 1296] >> setpagedevice"
*PageSize ARCHCEXT/C+ 18"x24" Oversize: 	"<< /PageSize [1296 1728] >> setpagedevice"
*PageSize ARCHDEXT/D+ 24"x36" Oversize: 	"<< /PageSize [1728 2592] >> setpagedevice"
*PageSize ARCHEEXT/E+ 36"x48" Oversize: 	"<< /PageSize [2592 3456] >> setpagedevice"
*PageSize A4EXT/A4 Oversize: 			"<< /PageSize [595 842]>> setpagedevice"
*PageSize A3EXT/A3 Oversize: 			"<< /PageSize [842 1191] >> setpagedevice"
*PageSize A2EXT/A2 Oversize: 			"<< /PageSize [1191 1684] >> setpagedevice"
*PageSize A1EXT/A1 Oversize: 			"<< /PageSize [1684 2384] >> setpagedevice"
*PageSize ISOB2EXT/B2 500x707mm Oversize: 	"<< /PageSize [1417 2004] >> setpagedevice"
*PageSize ISOB1EXT/B1 707x1000mm Oversize: 	"<< /PageSize [2004 2835] >> setpagedevice"
*PageSize inch30EXT/30"x42" Oversize:   	"<< /PageSize [2160 3024] >> setpagedevice"
*PageSize mm700EXT/700x1000mm Oversize: 	"<< /PageSize [1984 2835] >> setpagedevice"
*PageSize mm500EXT/500x700mm Oversize: 		"<< /PageSize [1417 1984] >> setpagedevice"
*?PageSize: "
  currentpagedevice /PageSize get aload aload pop gt {exch} if
  (Unknown) <<
   /A0 [2384 3370]
   /AnsiA [612 792]
   /AnsiB [792 1224]
   /AnsiC [1224 1584]
   /AnsiD [1584 2448]
   /AnsiE [2448 3168]
   /ARCHA [648 864]
   /ARCHB [864 1296]
   /ARCHC [1296 1728]
   /ARCHD [1728 2592]
   /ARCHE [2592 3456]
   /A4 [595 842]
   /A3 [842 1191]
   /A2 [1191 1684]
   /A1 [1684 2384]
   /ISOB2 [1417 2004]
   /ISOB1 [2004 2835]
   /inch30 [2160 3024]
   /mm700 [1984 2835]
   /mm500 [1417 1984]
   /A0EXT [2384 3370]
   /AnsiAEXT [612 792]
   /AnsiBEXT [792 1224]
   /AnsiCEXT [1224 1584]
   /AnsiDEXT [1584 2448]
   /AnsiEEXT [2448 3168]
   /ARCHAEXT [648 864]
   /ARCHBEXT [864 1296]
   /ARCHCEXT [1296 1728]
   /ARCHDEXT [1728 2592]
   /ARCHEEXT [2592 3456]
   /A4EXT [595 842]
   /A3EXT [842 1191]
   /A2EXT [1191 1684]
   /A1EXT [1684 2384]
   /ISOB2EXT [1417 2004]
   /ISOB1EXT [2004 2835]
   /inch30EXT [2160 3024]
   /mm700EXT [1984 2835]
   /mm500EXT [1417 1984]
  >>
  { aload aload pop gt {exch} if 4
    index sub abs 5 le exch 5 index sub abs 5 le and {exch} if pop
  } bind forall = pop pop"
*End
*CloseUI: *PageSize

*OpenUI *PageRegion: PickOne
*OrderDependency: 50.3 AnySetup *PageRegion
*DefaultPageRegion: A0
*PageRegion A0/A0: 				"<< /PageSize [2384 3370] >> setpagedevice"
*PageRegion AnsiA/A 8.5"x11": 			"<< /PageSize [612 792] >> setpagedevice"
*PageRegion AnsiB/B 11"x17": 			"<< /PageSize [792 1224] >> setpagedevice"
*PageRegion AnsiC/C 17"x22": 			"<< /PageSize [1224 1584] >> setpagedevice"
*PageRegion AnsiD/D 22"x34": 			"<< /PageSize [1584 2448] >> setpagedevice"
*PageRegion AnsiE/E 34"x44": 			"<< /PageSize [2448 3168] >> setpagedevice"
*PageRegion ARCHA/A+ 9"x12": 			"<< /PageSize [648 864] >> setpagedevice"
*PageRegion ARCHB/B+ 12"x18": 			"<< /PageSize [864 1296] >> setpagedevice"
*PageRegion ARCHC/C+ 18"x24": 			"<< /PageSize [1296 1728] >> setpagedevice"
*PageRegion ARCHD/D+ 24"x36": 			"<< /PageSize [1728 2592] >> setpagedevice"
*PageRegion ARCHE/E+ 36"x48": 			"<< /PageSize [2592 3456] >> setpagedevice"
*PageRegion A4/A4: 				"<< /PageSize [595 842]>> setpagedevice"
*PageRegion A3/A3: 				"<< /PageSize [842 1191] >> setpagedevice"
*PageRegion A2/A2: 				"<< /PageSize [1191 1684] >> setpagedevice"
*PageRegion A1/A1: 				"<< /PageSize [1684 2384] >> setpagedevice"
*PageRegion ISOB2/B2 500x707mm: 		"<< /PageSize [1417 2004] >> setpagedevice"
*PageRegion ISOB1/B1 707x1000mm: 		"<< /PageSize [2004 2835] >> setpagedevice"
*PageRegion inch30/30"x42":   			"<< /PageSize [2160 3024] >> setpagedevice"
*PageRegion mm700/700x1000mm: 			"<< /PageSize [1984 2835] >> setpagedevice"
*PageRegion mm500/500x700mm: 			"<< /PageSize [1417 1984] >> setpagedevice"
*PageRegion A0EXT/A0 Oversize: 			"<< /PageSize [2384 3370] >> setpagedevice"
*PageRegion AnsiAEXT/A 8.5"x11" Oversize: 	"<< /PageSize [612 792] >> setpagedevice"
*PageRegion AnsiBEXT/B 11"x17" Oversize: 	"<< /PageSize [792 1224] >> setpagedevice"
*PageRegion AnsiCEXT/C 17"x22" Oversize: 	"<< /PageSize [1224 1584] >> setpagedevice"
*PageRegion AnsiDEXT/D 22"x34" Oversize: 	"<< /PageSize [1584 2448] >> setpagedevice"
*PageRegion AnsiEEXT/E 34"x44" Oversize: 	"<< /PageSize [2448 3168] >> setpagedevice"
*PageRegion ARCHAEXT/A+ 9"x12" Oversize: 	"<< /PageSize [648 864] >> setpagedevice"
*PageRegion ARCHBEXT/B+ 12"x18" Oversize: 	"<< /PageSize [864 1296] >> setpagedevice"
*PageRegion ARCHCEXT/C+ 18"x24" Oversize: 	"<< /PageSize [1296 1728] >> setpagedevice"
*PageRegion ARCHDEXT/D+ 24"x36" Oversize: 	"<< /PageSize [1728 2592] >> setpagedevice"
*PageRegion ARCHEEXT/E+ 36"x48" Oversize: 	"<< /PageSize [2592 3456] >> setpagedevice"
*PageRegion A4EXT/A4 Oversize: 			"<< /PageSize [595 842]>> setpagedevice"
*PageRegion A3EXT/A3 Oversize: 			"<< /PageSize [842 1191] >> setpagedevice"
*PageRegion A2EXT/A2 Oversize: 			"<< /PageSize [1191 1684] >> setpagedevice"
*PageRegion A1EXT/A1 Oversize: 			"<< /PageSize [1684 2384] >> setpagedevice"
*PageRegion ISOB2EXT/B2 500x707mm Oversize:	"<< /PageSize [1417 2004] >> setpagedevice"
*PageRegion ISOB1EXT/B1 707x1000mm Oversize:	"<< /PageSize [2004 2835] >> setpagedevice"
*PageRegion inch30EXT/30"x42" Oversize:		"<< /PageSize [2160 3024] >> setpagedevice"
*PageRegion mm700EXT/700x1000mm Oversize: 	"<< /PageSize [1984 2835] >> setpagedevice"
*PageRegion mm500EXT/500x700mm Oversize: 	"<< /PageSize [1417 1984] >> setpagedevice"
*CloseUI: *PageRegion

*% ========== 5.15 Information About Media Sizes ============================================

*DefaultImageableArea: A0
*ImageableArea A0/A0: 					"14.0 14.0 2370.0 3356.0 "
*ImageableArea AnsiA/A 8.5"x11": 			"14.0 14.0 598.0 778.0 "
*ImageableArea AnsiB/B 11"x17": 			"14.0 14.0 778.0 1210.0 "
*ImageableArea AnsiC/C 17"x22": 			"14.0 14.0 1210.0 1570.0 "
*ImageableArea AnsiD/D 22"x34": 			"14.0 14.0 1570.0 2434.0 "
*ImageableArea AnsiE/E 34"x44": 			"14.0 14.0 2434.0 3154.0 "
*ImageableArea ARCHA/A+ 9"x12": 			"14.0 14.0 634.0 850.0 "
*ImageableArea ARCHB/B+ 12"x18": 			"14.0 14.0 850.0 1282.0 "
*ImageableArea ARCHC/C+ 18"x24": 			"14.0 14.0 1282.0 1714.0 "
*ImageableArea ARCHD/D+ 24"x36": 			"14.0 14.0 1714.0 2578.0 "
*ImageableArea ARCHE/E+ 36"x48": 			"14.0 14.0 2578.0 3442.0 "
*ImageableArea A4/A4: 					"14.0 14.0 581.0 828.0 "
*ImageableArea A3/A3: 					"14.0 14.0 828.0 1177.0 "
*ImageableArea A2/A2: 					"14.0 14.0 1177.0 1670.0 "
*ImageableArea A1/A1: 					"14.0 14.0 1670.0 2370.0 "
*ImageableArea ISOB2/B2 500x707mm: 			"14.0 14.0 1403.0 1990.0 "
*ImageableArea ISOB1/B1 707x1000mm: 			"14.0 14.0 1990.0 2821.0 "
*ImageableArea inch30/30"x42":   			"14.0 14.0 2146 3010"
*ImageableArea mm700/700x1000mm: 			"14.0 14.0 1970 2821"
*ImageableArea mm500/500x700mm: 			"14.0 14.0 1403 1970"
*ImageableArea A0EXT/A0 Oversize: 			"0.0 0.0 2384.0 3370.0 "
*ImageableArea AnsiAEXT/A 8.5"x11" Oversize: 		"0.0 0.0 612.0 792.0 "
*ImageableArea AnsiBEXT/B 11"x17" Oversize: 		"0.0 0.0 792.0 1224.0 "
*ImageableArea AnsiCEXT/C 17"x22" Oversize: 		"0.0 0.0 1224.0 1584.0 "
*ImageableArea AnsiDEXT/D 22"x34" Oversize:		"0.0 0.0 1584.0 2448.0 "
*ImageableArea AnsiEEXT/E 34"x44" Oversize: 		"0.0 0.0 2448.0 3168.0 "
*ImageableArea ARCHAEXT/A+ 9"x12" Oversize: 		"0.0 0.0 648.0 864.0 "
*ImageableArea ARCHBEXT/B+ 12"x18" Oversize: 		"0.0 0.0 864.0 1296.0 "
*ImageableArea ARCHCEXT/C+ 18"x24" Oversize: 		"0.0 0.0 1296.0 1728.0 "
*ImageableArea ARCHDEXT/D+ 24"x36" Oversize: 		"0.0 0.0 1728.0 2592.0 "
*ImageableArea ARCHEEXT/E+ 36"x48" Oversize: 		"0.0 0.0 2592.0 3456.0 "
*ImageableArea A4EXT/A4 Oversize: 			"0.0 0.0 595.0 842.0 "
*ImageableArea A3EXT/A3 Oversize: 			"0.0 0.0 842.0 1191.0 "
*ImageableArea A2EXT/A2 Oversize: 			"0.0 0.0 1191.0 1684.0 "
*ImageableArea A1EXT/A1 Oversize: 			"0.0 0.0 1684.0 2384.0 "
*ImageableArea ISOB2EXT/B2 500x707mm Oversize: 		"0.0 0.0 1417.0 2004.0 "
*ImageableArea ISOB1EXT/B1 707x1000mm Oversize: 	"0.0 0.0 2004.0 2835.0 "
*ImageableArea inch30EXT/30"x42" Oversize:   		"0.0 0.0 2160 3024"
*ImageableArea mm700EXT/700x1000mm Oversize: 		"0.0 0.0 1984 2835"
*ImageableArea mm500EXT/500x700mm Oversize: 		"0.0 0.0 1417 1984"
*?ImageableArea: "
  newpath clippath pathbbox 4 -2 roll
  exch 2 {10000 mul ceiling 10000 div dup 0 lt {pop 0.0} if 128 string cvs print ( ) print} repeat
  exch 2 {10000 mul floor   10000 div dup 0 lt {pop 0.0} if 128 string cvs print ( ) print} repeat
  (\n) print"
*End

*DefaultPaperDimension: A0
*PaperDimension A0/A0: 					"2384 3370"
*PaperDimension AnsiA/A 8.5"x11": 			"612 792"
*PaperDimension AnsiB/B 11"x17": 			"792 1224"
*PaperDimension AnsiC/C 17"x22": 			"1224 1584"
*PaperDimension AnsiD/D 22"x34": 			"1584 2448"
*PaperDimension AnsiE/E 34"x44": 			"2448 3168"
*PaperDimension ARCHA/A+ 9"x12": 			"648 864"
*PaperDimension ARCHB/B+ 12"x18": 			"864 1296"
*PaperDimension ARCHC/C+ 18"x24": 			"1296 1728"
*PaperDimension ARCHD/D+ 24"x36": 			"1728 2592"
*PaperDimension ARCHE/E+ 36"x48": 			"2592 3456"
*PaperDimension A4/A4: 					"595 842"
*PaperDimension A3/A3: 					"842 1191"
*PaperDimension A2/A2: 					"1191 1684"
*PaperDimension A1/A1: 					"1684 2384"
*PaperDimension ISOB2/B2 500x707mm: 			"1417 2004"
*PaperDimension ISOB1/B1 707x1000mm: 			"2004 2835"
*PaperDimension inch30/30"x42":   			"2160 3024"
*PaperDimension mm700/700x1000mm: 			"1984 2835"
*PaperDimension mm500/500x700mm: 			"1417 1984"
*PaperDimension A0EXT/A0 Oversize: 			"2384 3370"
*PaperDimension AnsiAEXT/A 8.5"x11" Oversize: 		"612 792"
*PaperDimension AnsiBEXT/B 11"x17" Oversize: 		"792 1224"
*PaperDimension AnsiCEXT/C 17"x22" Oversize: 		"1224 1584"
*PaperDimension AnsiDEXT/D 22"x34" Oversize: 		"1584 2448"
*PaperDimension AnsiEEXT/E 34"x44" Oversize: 		"2448 3168"
*PaperDimension ARCHAEXT/A+ 9"x12" Oversize: 		"648 864"
*PaperDimension ARCHBEXT/B+ 12"x18" Oversize: 		"864 1296"
*PaperDimension ARCHCEXT/C+ 18"x24" Oversize: 		"1296 1728"
*PaperDimension ARCHDEXT/D+ 24"x36" Oversize: 		"1728 2592"
*PaperDimension ARCHEEXT/E+ 36"x48" Oversize: 		"2592 3456"
*PaperDimension A4EXT/A4 Oversize: 			"595 842"
*PaperDimension A3EXT/A3 Oversize: 			"842 1191"
*PaperDimension A2EXT/A2 Oversize: 			"1191 1684"
*PaperDimension A1EXT/A1 Oversize: 			"1684 2384"
*PaperDimension ISOB2EXT/B2 500x707mm Oversize: 	"1417 2004"
*PaperDimension ISOB1EXT/B1 707x1000mm Oversize: 	"2004 2835"
*PaperDimension inch30EXT/30"x42" Oversize:   		"2160 3024"
*PaperDimension mm700EXT/700x1000mm Oversize: 		"1984 2835"
*PaperDimension mm500EXT/500x700mm Oversize: 		"1417 1984"

*RequiresPageRegion All: True

*% ========== 5.16 CustomPageSizes ==========================================================

*CustomPageSize True: "
	pop pop pop
	2 dict begin
	/PageSize [ 4 -2 roll ] def
	/ImagingBBox null def
	currentdict end setpagedevice
"
*End
*ParamCustomPageSize Width: 1 points 595 2592
*ParamCustomPageSize Height: 2 points 842 1800000
*ParamCustomPageSize WidthOffset: 3 points 0 1997
*ParamCustomPageSize HeightOffset: 4 points 0 1799158
*ParamCustomPageSize Orientation: 5 int 0 1
*MaxMediaWidth: "2592"
*MaxMediaHeight: "1800000"

*OpenUI *MediaType/Media type: PickOne
*OrderDependency: 50.4 AnySetup *MediaType
*DefaultMediaType: PrinterDefault

*MediaType PrinterDefault/Printer default: ""
*MediaType DraftPaper_80gr/Draft paper: 				"<< /MediaType (DraftPaper_80gm2) >> setpagedevice"
*MediaType StandardPaper_90gr/Standard paper: 		"<< /MediaType (StandardPaper_90gm2) >> setpagedevice"
*MediaType CheckBondUncoated_20lb/Bond: 			"<< /MediaType (CheckBondUncoated_20lbs) >> setpagedevice"
*MediaType DeluxBondUcoated_24lb/Deluxe bond: 		"<< /MediaType (DeluxBondUncoated_24lbs) >> setpagedevice"
*MediaType OcePremiumGrade_90gr/Premium coated paper: 	"<< /MediaType (PremiumGrade_90gm2) >> setpagedevice"
*MediaType ColorBondCoated_24lb/Coated bond: 		"<< /MediaType (ColorBondCoated_24lbs) >> setpagedevice"
*MediaType StabilizedPaper_90-110gr/Matt photo paper:	"<< /MediaType (StabilizedPaper_110gm2) >> setpagedevice"
*MediaType OcePhotogloss_175gr/High gloss photo paper:	"<< /MediaType (Photogloss_175gm2) >> setpagedevice"
*MediaType TranslucentBond_18lb/Translucent bond: 	"<< /MediaType (TranslucentBond_18lbs) >> setpagedevice"
*MediaType DoubleMattFilm_95mu/Matt film: 			"<< /MediaType (DoubleMattFilm_95mu) >> setpagedevice"
*MediaType TracingPaper_90gr/Tracing paper: 			"<< /MediaType (TracingPaper_90gm2) >> setpagedevice"
*MediaType ColorVellum_20lb/Color vellum: 			"<< /MediaType (ColorVellum_20lbs) >> setpagedevice"
*MediaType MonochromeVellum_20lb/Monochrome vellum: 	"<< /MediaType (MonochromeVellum_20lbs) >> setpagedevice"
*MediaType RecycledPaper_64gm2/Recycled plain: 		"<< /MediaType (RecycledPaper_64gm2) >> setpagedevice"
*MediaType TracingPaper_75gm2/Tracing (Japan): 		"<< /MediaType (TracingPaper_75gm2) >> setpagedevice"
*MediaType CoatedPaper_81gm2/Plain coated: 			"<< /MediaType (CoatedPaper_81gm2) >> setpagedevice"
*MediaType ThinMattFilm_50mu/Matt film (thin): 			"<< /MediaType (ThinMattFilm_50mu) >> setpagedevice"
*MediaType Custom1/Custom uncoated: 					"<< /MediaType (Custom1) >> setpagedevice"
*MediaType Custom2/Custom coated: 					"<< /MediaType (Custom2) >> setpagedevice"
*MediaType Custom3/Custom special: 					"<< /MediaType (Custom3) >> setpagedevice"
*CloseUI: *MediaType

*% ========== 5.17 Media Handling Features ==================================================

*OpenUI *TraySwitch/Roll Switch: Boolean
*OrderDependency: 50.5 AnySetup *TraySwitch
*DefaultTraySwitch: False
*TraySwitch True/On:  "<</TraySwitch true>> setpagedevice"
*TraySwitch False/Off: "<</TraySwitch false>> setpagedevice"
*?TraySwitch: "currentpagedevice /TraySwitch get {(True)} {(False)} ifelse ="
*CloseUI: *TraySwitch

*OpenUI *OutputMode/Quality: PickOne
*OrderDependency: 50.6 AnySetup *OutputMode
*DefaultOutputMode: normal
*OutputMode draft/Check: "<< /PreRenderingEnhance true  /PreRenderingEnhanceDetails << /Type 3 /Quality 1 >> >> setpagedevice"
*OutputMode normal/Release: "<< /PreRenderingEnhance true  /PreRenderingEnhanceDetails << /Type 3 /Quality 2 >> >> setpagedevice"
*OutputMode enhanced/Presentation: "<< /PreRenderingEnhance true  /PreRenderingEnhanceDetails << /Type 3 /Quality 3 >> >> setpagedevice"
*CloseUI: *OutputMode

*%OpenUI *OCContentType/Content Type: PickOne
*%OrderDependency: 50.7 AnySetup *OCContentType
*%DefaultOCContentType: areafills
*%OCContentType linestext/Text: "<< /PreRenderingEnhance true  /PreRenderingEnhanceDetails << /Type 3 /ContentType 1 >> >> setpagedevice"
*%OCContentType areafills/Area fill: "<< /PreRenderingEnhance true  /PreRenderingEnhanceDetails << /Type 3 /ContentType 3 >> >> setpagedevice"
*%CloseUI: *OCContentType

*%OpenUI *OCColorMode/Color Mode: PickOne
*%OrderDependency: 50.7 AnySetup *OCColorMode
*%DefaultOCColorMode: CMYK
*%OCColorMode CMYK/Color: "<< /ProcessColorModel /DeviceCMYK >> setpagedevice"
*%OCColorMode Gray/Grayscale: "<< /ProcessColorModel /DeviceGray >> setpagedevice"
*%CloseUI: *OCColorMode

*OpenGroup: *OCEColorManagement/Oc� Color Management

*OpenUI *OCColorMode/Color Mode: PickOne
*OrderDependency: 50.7 AnySetup *OCColorMode
*DefaultOCColorMode: CMYK
*OCColorMode CMYK/Color: "<< /ProcessColorModel /DeviceCMYK >> setpagedevice"
*OCColorMode Gray/Grayscale: "<< /ProcessColorModel /DeviceGray >> setpagedevice"
*CloseUI: *OCColorMode

*OpenUI *OCColorFeel/Oc� Color Feel: PickOne
*OrderDependency: 50.7 AnySetup *OCColorFeel
*DefaultOCColorFeel: PrinterDefault
*OCColorFeel None/Not Applicable: ""
*OCColorFeel PrinterDefault/Printer default: ""
*OCColorFeel InApplication/None, managed by application: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /RGBSpace 0 /CMYKSpace 0 >> >> setpagedevice"
*OCColorFeel OceCAD/Oc� CAD colors: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /RGBSpace 3 /CMYKSpace 8 /RenderingIntent 1 >> >> setpagedevice"
*OCColorFeel SimulatedOceTCS400/Simulated Oc� TCS400 CAD colors: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 1 /RGBSpace 3 /CMYKSpace 8 /RenderingIntent 1 >> >> setpagedevice"
*OCColorFeel SimulatedVivid/Simulated device, vivid colors: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 2 /RGBSpace 1 /CMYKSpace 8 /RenderingIntent 1 >> >> setpagedevice"
*OCColorFeel SimulatedMatchScreenColors/Simulated device, match screen colors: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 2 /RGBSpace 1 /CMYKSpace 8 /RenderingIntent 0 >> >> setpagedevice" 
*OCColorFeel SimulatedNoCorrection/Simulated device, no color correction: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 2 /RGBSpace 3 /CMYKSpace 8 /RenderingIntent 1 >> >> setpagedevice"
*OCColorFeel OCEnhancedColors/Oc� enhanced colors, set 1),2),3): "" 
*CloseUI: *OCColorFeel

*CloseGroup: *OCEColorManagement

*OpenGroup: *OceEnhancedColors/Oc� enhanced colors

*OpenSubGroup: *RGBInputData/RGB input data

*OpenUI *OCRGBInputData/1)RGB input data:PickOne
*OrderDependency: 50.7 AnySetup *OCRGBInputData
*DefaultOCRGBInputData: None
*OCRGBInputData None/Not Applicable: ""
*OCRGBInputData sRGB/sRGB: "<< /Type 3 /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /RGBSpace 1 >> >> setpagedevice"
*OCRGBInputData AdobeRGB/Adobe RGB: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /RGBSpace 2 >> >> setpagedevice"
*CloseUI: *OCRGBInputData

*CloseSubGroup: *RGBInputData

*OpenSubGroup: *CMYKInputData/CMYK input data

*OpenUI *OCCMYKInputData/2)CMYK input data:PickOne
*OrderDependency: 50.7 AnySetup *OCCMYKInputData
*DefaultOCCMYKInputData: None
*OCCMYKInputData None/Not Applicable: ""
*OCCMYKInputData EuroscaleCoated/Euroscale coated: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /CMYKSpace 4 >> >> setpagedevice"
*OCCMYKInputData EuroscaleUncoated/Euroscale uncoated: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /CMYKSpace 5 >> >> setpagedevice"
*OCCMYKInputData USWebCoated/US web coated SWOP: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /CMYKSpace 6 >> >> setpagedevice"
*OCCMYKInputData USWebUncoated/US web uncoated: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /CMYKSpace 7 >> >> setpagedevice"
*CloseUI: *OCCMYKInputData

*CloseSubGroup: *CMYKInputData

*OpenSubGroup: *OCRenderIntent/Oc� Rendering intent

*OpenUI *OCRenderingIntent/3)Oc� rendering intent: PickOne
*OrderDependency: 50.7 AnySetup *OCRenderingIntent
*DefaultOCRenderingIntent: None
*OCRenderingIntent None/Not Applicable: ""
*OCRenderingIntent Perceptual/Perceptual (photo): "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /RenderingIntent 0 >> >> setpagedevice"
*OCRenderingIntent Saturation/Saturation (business graphics): "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /RenderingIntent 1 >> >> setpagedevice"
*OCRenderingIntent RelativeColorimetric/Relative colorimetric: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /RenderingIntent 2 >> >> setpagedevice"
*OCRenderingIntent AbsoluteColorimetric/Absolute colorimetric: "<< /PreRenderingEnhance true
/PreRenderingEnhanceDetails << /Type 3 /Simulation 0 /RenderingIntent 3 >> >> setpagedevice"
*CloseUI: *OCRenderingIntent

*CloseSubGroup: *OCRenderIntent

*CloseGroup: *OceEnhancedColors

*% ========== 5.18 Finishing Features =======================================================

*OpenGroup: Finishing/Finishing

*OpenUI *Collate: Boolean
*OrderDependency: 50.9 AnySetup *Collate
*DefaultCollate: True
*Collate True:  "<< /Collate true>> setpagedevice"
*Collate False: "<< /Collate false>>setpagedevice"
*?Collate: "currentpagedevice /Collate get {(True)} {(False)} ifelse ="
*CloseUI: *Collate

*OpenUI *FoldWhen/Folding: PickOne
*OrderDependency: 50.9 AnySetup *FoldWhen
*DefaultFoldWhen: None
*FoldWhen None/Off: "<</Fold 0>> setpagedevice"
*FoldWhen EndOfPage/On: "<</Fold 4>> setpagedevice"
*?FoldWhen: "   5 dict begin
  0 (None) def
  1 (DeviceDeactivation) def
  2 (EndOfJob) def
  3 (EndOfSet) def
  4 (EndOfPage) def
  currentpagedevice /Fold get load end ="
*End
*CloseUI: *FoldWhen

*CloseGroup: Finishing

*% ========== 5.19 Imagesetter Features =====================================================

*% ========== 5.20 Font Related Keywords ====================================================

*FCacheSize 50meg:100000

*Font AdobeSansMM: Standard "(001.002)" Standard ROM
*Font AdobeSerifMM: Standard "(001.003)" Standard ROM
*Font AlbertusMT: Standard "(001.001)" Standard ROM
*Font AlbertusMT-Italic: Standard "(001.001)" Standard ROM
*Font AlbertusMT-Light: Standard "(001.001)" Standard ROM
*Font AntiqueOlive-Bold: Standard "(001.002)" Standard ROM
*Font AntiqueOlive-Compact: Standard "(001.002)" Standard ROM
*Font AntiqueOlive-Italic: Standard "(001.002)" Standard ROM
*Font AntiqueOlive-Roman: Standard "(001.002)" Standard ROM
*Font Apple-Chancery: Standard "(3.0)" Standard ROM
*Font Arial-BoldItalicMT: Standard "(001.003)" Standard ROM
*Font Arial-BoldMT: Standard "(001.003)" Standard ROM
*Font Arial-ItalicMT: Standard "(001.003)" Standard ROM
*Font ArialMT: Standard "(001.003)" Standard ROM
*Font AvantGarde-Book: Standard "(003.000)" Standard ROM
*Font AvantGarde-BookOblique: Standard "(003.000)" Standard ROM
*Font AvantGarde-Demi: Standard "(003.000)" Standard ROM
*Font AvantGarde-DemiOblique: Standard "(003.000)" Standard ROM
*Font Bodoni: Standard "(001.003)" Standard ROM
*Font Bodoni-Bold: Standard "(001.003)" Standard ROM
*Font Bodoni-BoldItalic: Standard "(001.003)" Standard ROM
*Font Bodoni-Italic: Standard "(001.003)" Standard ROM
*Font Bodoni-Poster: Standard "(001.003)" Standard ROM
*Font Bodoni-PosterCompressed: Standard "(001.002)" Standard ROM
*Font Bookman-Demi: Standard "(003.000)" Standard ROM
*Font Bookman-DemiItalic: Standard "(003.000)" Standard ROM
*Font Bookman-Light: Standard "(003.000)" Standard ROM
*Font Bookman-LightItalic: Standard "(003.000)" Standard ROM
*Font Carta: Special "(001.001)" Special ROM
*Font Chicago: Standard "(3.0)" Standard ROM
*Font Clarendon: Standard "(001.002)" Standard ROM
*Font Clarendon-Bold: Standard "(001.002)" Standard ROM
*Font Clarendon-Light: Standard "(001.002)" Standard ROM
*Font CooperBlack: Standard "(001.004)" Standard ROM
*Font CooperBlack-Italic: Standard "(001.004)" Standard ROM
*Font Copperplate-ThirtyThreeBC: Standard "(001.003)" Standard ROM
*Font Copperplate-ThirtyTwoBC: Standard "(001.003)" Standard ROM
*Font Coronet-Regular: Standard "(001.002)" Standard ROM
*Font Courier: Standard "(004.000)" Standard ROM
*Font Courier-Bold: Standard "(004.000)" Standard ROM
*Font Courier-BoldOblique: Standard "(004.000)" Standard ROM
*Font Courier-Oblique: Standard "(004.000)" Standard ROM
*Font Eurostile: Standard "(001.003)" Standard ROM
*Font Eurostile-Bold: Standard "(001.002)" Standard ROM
*Font Eurostile-BoldExtendedTwo: Standard "(001.003)" Standard ROM
*Font Eurostile-ExtendedTwo: Standard "(001.003)" Standard ROM
*Font Geneva: Standard "(3.0)" Standard ROM
*Font GillSans: Standard "(001.003)" Standard ROM
*Font GillSans-Bold: Standard "(001.002)" Standard ROM
*Font GillSans-BoldCondensed: Standard "(001.002)" Standard ROM
*Font GillSans-BoldItalic: Standard "(001.003)" Standard ROM
*Font GillSans-Condensed: Standard "(001.002)" Standard ROM
*Font GillSans-ExtraBold: Standard "(001.002)" Standard ROM
*Font GillSans-Italic: Standard "(001.003)" Standard ROM
*Font GillSans-Light: Standard "(001.002)" Standard ROM
*Font GillSans-LightItalic: Standard "(001.003)" Standard ROM
*Font Goudy: Standard "(001.004)" Standard ROM
*Font Goudy-Bold: Standard "(001.003)" Standard ROM
*Font Goudy-BoldItalic: Standard "(001.003)" Standard ROM
*Font Goudy-ExtraBold: Standard "(001.002)" Standard ROM
*Font Goudy-Italic: Standard "(001.003)" Standard ROM
*Font Helvetica: Standard "(003.000)" Standard ROM
*Font Helvetica-Bold: Standard "(003.000)" Standard ROM
*Font Helvetica-BoldOblique: Standard "(003.000)" Standard ROM
*Font Helvetica-Condensed: Standard "(003.000)" Standard ROM
*Font Helvetica-Condensed-Bold: Standard "(003.000)" Standard ROM
*Font Helvetica-Condensed-BoldObl: Standard "(003.000)" Standard ROM
*Font Helvetica-Condensed-Oblique: Standard "(003.000)" Standard ROM
*Font Helvetica-Narrow: Standard "(003.000)" Standard ROM
*Font Helvetica-Narrow-Bold: Standard "(003.000)" Standard ROM
*Font Helvetica-Narrow-BoldOblique: Standard "(003.000)" Standard ROM
*Font Helvetica-Narrow-Oblique: Standard "(003.000)" Standard ROM
*Font Helvetica-Oblique: Standard "(003.000)" Standard ROM
*Font HoeflerText-Black: Standard "(1.0)" Unknown ROM
*Font HoeflerText-BlackItalic: Standard "(1.0)" Unknown ROM
*Font HoeflerText-Italic: Standard "(1.0)" Unknown ROM
*Font HoeflerText-Ornaments: Special "(001.001)" Standard ROM
*Font HoeflerText-Regular: Standard "(1.0)" Unknown ROM
*Font JoannaMT: Standard "(001.001)" Standard ROM
*Font JoannaMT-Bold: Standard "(001.001)" Standard ROM
*Font JoannaMT-BoldItalic: Standard "(001.001)" Standard ROM
*Font JoannaMT-Italic: Standard "(001.001)" Standard ROM
*Font LetterGothic: Standard "(001.005)" Standard ROM
*Font LetterGothic-Bold: Standard "(001.007)" Standard ROM
*Font LetterGothic-BoldSlanted: Standard "(001.006)" Standard ROM
*Font LetterGothic-Slanted: Standard "(001.005)" Standard ROM
*Font LubalinGraph-Book: Standard "(001.004)" Standard ROM
*Font LubalinGraph-BookOblique: Standard "(001.004)" Standard ROM
*Font LubalinGraph-Demi: Standard "(001.004)" Standard ROM
*Font LubalinGraph-DemiOblique: Standard "(001.004)" Standard ROM
*Font Marigold: Standard "(001.001)" Standard ROM
*Font Monaco: Standard "(3.0)" Standard ROM
*Font MonaLisa-Recut: Standard "(001.001)" Standard ROM
*Font NewCenturySchlbk-Bold: Standard "(003.000)" Standard ROM
*Font NewCenturySchlbk-BoldItalic: Standard "(003.000)" Standard ROM
*Font NewCenturySchlbk-Italic: Standard "(003.000)" Standard ROM
*Font NewCenturySchlbk-Roman: Standard "(003.000)" Standard ROM
*Font NewYork: Standard "(001.000)" ExtendedRoman ROM
*Font Optima: Standard "(001.006)" Standard ROM
*Font Optima-Bold: Standard "(001.007)" Standard ROM
*Font Optima-BoldItalic: Standard "(001.001)" Standard ROM
*Font Optima-Italic: Standard "(001.001)" Standard ROM
*Font Oxford: Standard "(001.001)" Standard ROM
*Font Palatino-Bold: Standard "(003.000)" Standard ROM
*Font Palatino-BoldItalic: Standard "(003.000)" Standard ROM
*Font Palatino-Italic: Standard "(003.000)" Standard ROM
*Font Palatino-Roman: Standard "(003.000)" Standard ROM
*Font StempelGaramond-Bold: Standard "(001.003)" Standard ROM
*Font StempelGaramond-BoldItalic: Standard "(001.003)" Standard ROM
*Font StempelGaramond-Italic: Standard "(001.003)" Standard ROM
*Font StempelGaramond-Roman: Standard "(001.003)" Standard ROM
*Font Symbol: Special "(001.008)" Special ROM
*Font Tekton: Standard "(001.002)" Standard ROM
*Font Times-Bold: Standard "(003.000)" Standard ROM
*Font Times-BoldItalic: Standard "(003.000)" Standard ROM
*Font Times-Italic: Standard "(003.000)" Standard ROM
*Font TimesNewRomanPS-BoldItalicMT: Standard "(001.003)" Standard ROM
*Font TimesNewRomanPS-BoldMT: Standard "(001.004)" Standard ROM
*Font TimesNewRomanPS-ItalicMT: Standard "(001.003)" Standard ROM
*Font TimesNewRomanPSMT: Standard "(001.003)" Standard ROM
*Font Times-Roman: Standard "(003.000)" Standard ROM
*Font Univers: Standard "(001.004)" Standard ROM
*Font Univers-Bold: Standard "(001.004)" Standard ROM
*Font Univers-BoldExt: Standard "(001.001)" Standard ROM
*Font Univers-BoldExtObl: Standard "(001.001)" Standard ROM
*Font Univers-BoldOblique: Standard "(001.004)" Standard ROM
*Font Univers-Condensed: Standard "(001.003)" Standard ROM
*Font Univers-CondensedBold: Standard "(001.002)" Standard ROM
*Font Univers-CondensedBoldOblique: Standard "(001.002)" Standard ROM
*Font Univers-CondensedOblique: Standard "(001.003)" Standard ROM
*Font Univers-Extended: Standard "(001.001)" Standard ROM
*Font Univers-ExtendedObl: Standard "(001.001)" Standard ROM
*Font Univers-Light: Standard "(001.004)" Standard ROM
*Font Univers-LightOblique: Standard "(001.004)" Standard ROM
*Font Univers-Oblique: Standard "(001.004)" Standard ROM
*Font Wingdings: Unknown "(001.001)" Unknown ROM
*Font Wingdings-Regular: Special "(Version 2.00)" Special ROM
*Font ZapfChancery-MediumItalic: Standard "(003.000)" Standard ROM
*Font ZapfDingbats: Special "(002.000)" Special ROM

*DefaultFont: Courier

*?FontQuery: "
 save
   { count 1 gt
      { exch dup 127 string cvs (/) print print (:) print
        /Font resourcestatus {pop pop (Yes)} {(No)} ifelse =
      } { exit } ifelse 
   } bind loop
   (*) = flush
 restore"
*End

*?FontList: "
save
  (*) {cvn ==} 128 string /Font resourceforall
  (*) = flush
restore
"
*End

*% ========== 5.21 Printer Messages =========================================================

*% ========== 5.22 Color Separation Keywords ================================================

*%?*DefaultColorSep
*%?ColorSepInfo [[600 600]]

*% End of PPD file for Oce_printer
