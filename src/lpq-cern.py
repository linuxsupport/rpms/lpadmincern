#!/usr/bin/python3 -W ignore::DeprecationWarning

#
# lpq-xprint: integration of CUPS and CERN xprint
#

import sys
import os
import getopt
import time
import subprocess
# Just so we do not confuse module name with function
from socket import *

testserverfile = "/usr/share/printconf/cern/PrintTests"

lpqcups = "/usr/bin/lpq.cups"
lpstatcups = "/usr/bin/lpstat.cups"
lpqargs = [" "]
lpqargsstr = " "
lpdomain = ".cern.ch"
lpserver = ".print"+lpdomain

for a in sys.argv[1:]:
  lpqargs.append(a)
  lpqargsstr += a
  lpqargsstr += " "

# default printer finding: $LPDEST -> $PRINTER -> lpstat -d
def get_default_printer(lpstat):
  if "LPDEST" in os.environ:
    return os.environ["LPDEST"]
  else:
    if "PRINTER" in os.environ:
      return os.environ["PRINTER"]
    else:
      try:
        output = os.popen("LC_ALL=C " + lpstat + " -d").readlines()[0]
        return output.split(": ")[1].strip()
      except Exception as e:
        print(e)
        pass
  return ""

# check remote lpr server for result
# NEED more error handling


def query_lpr(printserver, msg):

  ## This is to check that given printer is defined in DNS
  ## which is then assumed to be managed by central service
  ## otherwise we service local printer
  try:
    gethostbyname(printserver)
  except Exception as e:
    print(e)
    pass
    return

  try:
    s = socket(AF_INET, SOCK_STREAM)
    s.settimeout(3)
    s.connect((printserver, 515))
    s.send(msg.encode())
    out = s.recv(8192)
    sys.stdout.write(out.decode())
  except KeyboardInterrupt:
    sys.exit(0)
    pass
  except Exception as e:
    sys.stderr.write("lp: error - connecting to: " + printserver + ")\n")
    sys.exit(1)
    pass
  s.close()
  return

# check local CUPS server
# NEED more error handling


def query_cups(lpqcomm, lpqargs):
  p = subprocess.Popen([lpqcomm, lpqargs], stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE, close_fds=True, universal_newlines=True)
  sout, serr = p.communicate()
  print(sout)
  if serr.startswith("lpq: Unknown destination"):
    sys.stderr.write(serr)
    sys.exit(1)
  if serr.startswith("lp: error - PRINTER environment"):
    sys.stderr.write(serr)
    sys.exit(1)
  if serr.startswith("lp: error - LPDEST environment"):
    sys.stderr.write(serr)
    sys.exit(1)


# Construct lpq message to pass to lpr server,
# according to RFC 1179
# \003 | Queue | SP | List | LF
# \004 | Queue | SP | List | LF
#
def make_lpr_msg(long, printer, user, new):
  if long:
    msg = "\004"
  else:
    msg = "\003"

  msg += printer

  if user != '':
    msg += " " + user

  msg += "\n"

  return msg


def checkiftest(printer, server):
  try:
    os.stat(testserverfile)
    fH = os.popen("/usr/bin/lpstat -v " + printer)
    for l in fH.readlines():
      srv = l.split('//')[1]
      srv = srv.split('/')[0]
      break
    gethostbyname(srv)
    if srv == None or srv == "":
      return (False, printer + server)
  except Exception as e:
    return (False, printer + server)

  print("Info: Using TEST print server: " + srv)
  return (True, srv)


# If we are not at CERN pass all handling to CUPS asap
if not getfqdn().endswith(lpdomain):
  os.execv(lpqcups, lpqargs)

try:
  opts, args = getopt.getopt(sys.argv[1:], "EP:la", [])
except getopt.GetoptError:
  print("Usage: lpq [ -E ] [ -P destination ] [ -l ] [ -a ] [ +interval ]")
  sys.exit(1)

long = 0
interval = 0
user = ''
printer = ''

for option, argument in opts:
  if option == "-P":
    printer = str(argument)
  if option == "-l":
    long = 1
  if option == "-a":
    print("Info: -a is not implemented (due to large number of print queues).")

if printer == '':
  printer = get_default_printer(lpstatcups)

(new, server) = checkiftest(printer, lpserver)

if interval:
  while 1:
    try:
      query_cups(lpqcups, lpqargsstr)
      query_lpr(server, make_lpr_msg(long, printer, user, new))
      time.sleep(interval)
    except KeyboardInterrupt:
      sys.exit(0)
      pass
else:
  print
  query_cups(lpqcups, lpqargsstr)
  query_lpr(server, make_lpr_msg(long, printer, user, new))
